var axios = require('axios');

let token = 'bWVpbnRAbmlldXdib3V3b25saW5lLm5sOmVRTFlqMW1LZElPcTh3QnFoVTlON0JGRA==';

const jiraRequest = axios.create({
    baseURL: 'https://nbonline.atlassian.net',
    headers: {
        'Authorization': 'Basic ' + token,
        'Accept': 'application/json',
    }
});

const getPendingTickets = () => {

    return new Promise(async (resolve) => {

        const query = 'project IN ("NWN", "NW2") AND status IN ("Ready to merge","Acceptance Tests 2") ORDER BY created DESC'
        // const query = 'project IN ("NWN", "NW2") AND status IN ("Code Review","Feedback","In Progress","Ready to merge","Acceptance Tests 2") ORDER BY created DESC'
        const maxResults = 99999;
        const fields = 'key'

        const {data} = await jiraRequest.get(`/rest/api/2/search?fields=${fields}&maxResults=${maxResults}&jql=${query}`)
        // console.log(data)
        resolve(data.issues.map(ticket => {
            return ticket.key
        }));
    });
}


const getPendingReleases = async () => {

    const projectName = 'NWN';
    const {data} = await jiraRequest.get(`/rest/api/3/project/${projectName}/version?status=unreleased`)
    return data.values.map(release => release.name);
}

module.exports = { getPendingTickets, getPendingReleases }
