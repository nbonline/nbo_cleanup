
const axios = require("axios");

// https://cloud.digitalocean.com/account/api/tokens?i=345073
const token = 'dop_v1_05dc364d863689e74aeeef2b541bd0ec1484484a342152690b652d0449413db9'

const selenoRequest = axios.create({
    baseURL: 'https://api.digitalocean.com',
    headers: {
        'Authorization': 'Bearer ' + token,
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
});

const getClusters = async () => {
    const {data} = await selenoRequest.get('/v2/kubernetes/clusters')
    return data;
}



const getNodePools = async (cluster) => {
    const {data} = await selenoRequest.get(`/v2/kubernetes/clusters/${cluster}/node_pools`)
    return data;
}

const getNodePool = async (cluster, nodePool) => {
    const {data} = await selenoRequest.get(`/v2/kubernetes/clusters/${cluster}/node_pools/${nodePool}`)
    return data;
}


const getPods = async (cluster) => {

    const {data} = await selenoRequest.get(`https://cloud.digitalocean.com/kubernetes/clusters/${cluster}/db/34507330-fbb7-4805-9562-7e2436607c8f/api/v1/pod/review`)
    return data;
}

const deleteRepository = async (pipeline) => {
    console.log('Deleting pipeline', pipeline)
    await selenoRequest.delete(`/api/repositories/${pipeline}`)
}

module.exports = { getClusters, getPods, deleteRepository, getNodePools, getNodePool }
