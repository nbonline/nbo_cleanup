
const axios = require("axios");

const token = 'bWVpbnRzcGFuOjNzNVBhXiNxZUppM3o2'

const selenoRequest = axios.create({
    baseURL: 'https://harbor.seleno.nl',
    headers: {
        'Authorization': 'Basic ' + token,
        'Accept': 'application/json',
    }
});

const getRepositories = async () => {
    const {data} = await selenoRequest.get('/api/repositories?project_id=34')
    return data;
}

const deleteRepository = async (pipeline) => {
    console.log('Deleting pipeline', pipeline)
    await selenoRequest.delete(`/api/repositories/${pipeline}`)
}

module.exports = { getRepositories, deleteRepository }
