

const { exec } = require('child_process');



const getPods = (ignoreList) => new Promise((resolve, reject) => {

    exec('kubectl get pods -n review -o go-template --template \'{{range .items}}{{.metadata.name}}{{";"}}{{end}}\'', (err, stdout, stderr) => {
        if (err) {
            // node couldn't execute the command
            console.log(`stderr: ${stderr}`);
            reject(stderr)
            return 'error';
        }

        const items = stdout.split(';');

        const matchedSites = items.filter(item => {
            const isInIgnoreList = ignoreList.find(ign => item.startsWith(ign));
            return !isInIgnoreList && item.length > 0;
        });

        resolve(matchedSites)

    });
})

module.exports = { getPods }
