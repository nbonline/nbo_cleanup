#! /usr/bin/env node




const {getPendingTickets} = require("./jira");
const {getPods} = require("./kubectl");



const ignoreList = [
    'rabbitmq-controller',
    'nbo-platform-release',
    'nbo-platform-develop',
    'nbo-platform-staging',
];

const tickets = async () => {

    const pods = await getPods(ignoreList)

    console.log(pods);
    const tickets = await getPendingTickets();
    tickets.forEach(ticket => {
        // Remove pods containing this ticket
        //nbo-platform-(hotfix-|feature-|epic-|bugfix-|feature-sumedia-)?nw2-41-.+
        console.log(ticket)
    })


}

tickets();


