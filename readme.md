
# Review environment cleanup

This script checks for old review repositories and removes them. 
an repository is old when;

- it starts with nbonline/release and the release version is already released
- it starts with nbonline/(feature|hotfix|bugfix) and the ticket number in jira is done

Make sure you put the right jira ticket nr in the branch name, start with 'feature/', 'hotfix/' or 'bugfix/' and the release branch name starts with ‘release/’. If not, the review environment will remove it after 10 days, even if its not done yet.

The repository nbonline/develop won't be removed



