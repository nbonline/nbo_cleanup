#!/bin/bash

## Requirements:
# - coreutils
# - kubectl
# stop the script when failed
set -e

#gdate --version
#kubectl version

# Write pods to pods.json
#kubectl -n review get pods -o json > pods.json

#kubectl get pods -n review

#kubectl -n review get pod nbo-platform-staging-web03-86c555fcdb-pf92s -o yaml
kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}' | awk '$2 <= "'$(date -d'now-4 hours' -Ins --utc | sed 's/+0000/Z/')'" { print $1 }' | xargs --no-run-if-empty kubectl delete pod
kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}' | awk '$2 <= "'$(date -d'now-4 hours' -Ins --utc | sed 's/+0000/Z/')'" { print $1 }'
kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}' | awk '$2 <= "'$(gdate -d '4 days ago' -Ins | sed 's/+0000/Z/')'" { print $1 }'
kubectl get pods -n review | awk 'match($5,/[0-9]+d/) {print $2}' | awk '$1 ~ /nbo-platform-feature-/{ print $1 }'
kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}' | awk '$1 ~ /nbo-platform-feature-/{ print $2 }'
kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}' | awk '$2 <= /nbo-platform-feature-/{ print $2 }'



kubectl delete deployment -n review $(
  kubectl get deployments -n review -o=custom-columns=NAME:.metadata.name,AGE:.metadata.creationTimestamp --no-headers=true | \
  awk '$1 ~ /nbo-platform-feature-/{ print $1 }' \
  )

kubectl delete pod -n review $(
  kubectl get pods -n review -o=custom-columns=NAME:.metadata.name,AGE:.metadata.creationTimestamp --no-headers=true | \
  awk '$1 ~ /nbo-platform-feature-/{ print $1 }' \
  )



kubectl get deployments -n review -o=custom-columns=NAME:.metadata.name,AGE:.metadata.creationTimestamp --no-headers=true --sort-by=.metadata.creationTimestamp | awk '$2 <= "'$(gdate -d '10 days ago' -Ins | sed 's/+0000/Z/')'" { print $1 }'

#kubectl delete pod -n foo $(kubectl get pods -n foo -o=custom-columns=NAME:.metadata.name,AGE:.metadata.creationTimestamp --no-headers=true --sort-by=.metadata.creationTimestamp | awk '$2 <= "'$(gdate -d '10 days ago' -Ins | sed 's/+0000/Z/')'" { print $1 }')
