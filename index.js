#! /usr/bin/env node

const {getPods, deleteRepository, getClusters, getNodePools, getNodePool} = require('./hosts/digitalOcean')
// const {getRepositories, deleteRepository} = require('./hosts/seleno')
const { getPendingTickets, getPendingReleases } = require('./jira')
const moment = require("moment");

const init = async () => {


    // const clusters = await getClusters()

    const cluster = "866c3422-0210-4e8d-8075-4091294130d3";
    // const nodePools = await getNodePools(cluster)// seleno repositories
    const nodePool = '30216c7c-d47d-45d9-b3fc-82129dbaf4b7';
    const node = '5211c3b5-11f8-4b58-a2d5-c0bf33cf5f4b';
    // const repositories = await getRepositories(cluster)// seleno repositories
    // const nodePoolsData = await getNodePools(cluster)
    const nodePoolData = await getPods(cluster)// seleno repositories
    console.log(JSON.stringify(nodePoolData))
    return;
    const tickets = await getPendingTickets(); // jira tickets
    const releases = await getPendingReleases(); // jira releases

    const ignoreList = [
        'nbonline/develop',
        'nbonline/develop-web03',
        'nbonline/staging-web03',
        'nbonline/staging-nwn',
    ]

    const ticketRegex = /nbonline\/(?:feature|bugfix|hotfix)-(nwn-\d+)/m;
    const releaseRegex = /nbonline\/release-(\d+-\d+-\d+)/;
    const deleteRepositories = []

    const tenDaysAgo = moment().subtract(10, 'days');

    repositories.forEach(repository => {

        const isOld = moment(repository.update_time).isBefore(tenDaysAgo)
        if (isOld) {
            let match;
            if ((match = ticketRegex.exec(repository.name)) !== null) {
                const [, ticket] = match
                const ticketName = ticket.toUpperCase();
                if (tickets.indexOf(ticketName) === -1) {
                    deleteRepositories.push(repository.name)
                }
            } else if ((match = releaseRegex.exec(repository.name)) !== null) {
                const [, release] = match
                const releaseName = release.replaceAll('-', '.');
                if (releases.indexOf(releaseName) === -1) {
                    deleteRepositories.push(repository.name)
                }
            } else if (ignoreList.indexOf(repository.name) === -1) { // not in ignore list
                deleteRepositories.push(repository.name)
            }
        }
    })

    console.log(deleteRepositories)
    // deleteRepositories.forEach(pipeline => deleteRepository(pipeline))
}

init();
