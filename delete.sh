#!/bin/bash

## Description
# Deletes the pod including deployment, service and ingress
## Requirements:
# - coreutils
# - kubectl

# stop the script when failed
set -e

export APP_NAME=$1;
echo "Removing branch x '$APP_NAME'..."

#kubectl -n review delete deployment $APP_NAME --ignore-not-found=true
#kubectl -n review delete service $APP_NAME --ignore-not-found=true
#kubectl -n review delete ingress $APP_NAME --ignore-not-found=true
#kubectl -n review delete pod $APP_NAME --ignore-not-found=true
#kubectl -n review delete pod $APP_NAME --force
#kubectl delete secret $APP_NAME-tls --ignore-not-found=true
#kubectl delete secret $APP_NAME-env --ignore-not-found=true
