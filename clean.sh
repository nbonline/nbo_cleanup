#!/bin/bash
## Description
# Get all pods of namespace review
# starting with nbo-platform-feature-
# of a surtain age
# pass it to delete.sh

## Requirements:
# - coreutils
# - kubectl
# stop the script when failed
set -e

#gdate --version
#kubectl version
AGE=$(gdate -d '10 days ago' -Ins | sed 's/+0000/Z/')

kubectl get deployments -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}'\
 | awk '$1 ~ /nbo-platform-feature-/' \
 | awk '$2 <= "'$AGE'" { printf "kubectl -n review delete deployment %s --ignore-not-found=true\n", $1 }' | bash

kubectl get services -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}'\
 | awk '$1 ~ /nbo-platform-feature-/ { print $1}' \
 | awk '$2 <= "'$AGE'" { printf "kubectl -n review delete service %s --ignore-not-found=true\n", $1 }' | bash

kubectl get ing -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}'\
 | awk '$1 ~ /nbo-platform-feature-/ { print $1}' \
 | awk '$2 <= "'$AGE'" { printf "kubectl -n review delete ing %s --ignore-not-found=true\n", $1 }' | bash

 kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}'\
  | awk '$1 ~ /nbo-platform-feature-/ { print $1}' \
  | awk '$2 <= "'$AGE'" { printf "kubectl -n review delete pod %s --ignore-not-found=true\n", $1 }' | bash
---

 kubectl get pods -n review -o go-template --template '{{range .items}}{{.metadata.name}} {{.metadata.creationTimestamp}}{{"\n"}}{{end}}'\
  | awk '$1 ~ /nbo-platform-feature-/ { print $1} ' \
  | awk '$2 <= "'$AGE'" { print $1 }'
